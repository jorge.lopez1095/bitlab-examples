package sv.edu.bitlab.test.mvp

class OperatorPresenter(private val view: Operator.View): Operator.Presenter {

    private var model: OperatorModel = OperatorModel(this)

    override fun showResult(result: String) {
        view.showResult(result)
    }

    override fun operation(input: String) {
        model.operation(input)
    }

    override fun showError(error: String) {
        view.showError(error)
    }

}