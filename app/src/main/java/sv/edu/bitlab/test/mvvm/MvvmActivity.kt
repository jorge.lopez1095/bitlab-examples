package sv.edu.bitlab.test.mvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import sv.edu.bitlab.test.R

class MvvmActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mvvm)
    }
}
