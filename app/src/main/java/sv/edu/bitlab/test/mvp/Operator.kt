package sv.edu.bitlab.test.mvp

interface Operator {

    interface View {
        fun showResult(result: String)
        fun showError(error: String)
    }

    interface Presenter {
        fun showResult(result: String)
        fun operation(input: String)
        fun showError(error: String)
    }

    interface Model {
        fun operation(input: String)
    }

}