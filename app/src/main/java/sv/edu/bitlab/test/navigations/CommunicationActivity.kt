package sv.edu.bitlab.test.navigations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import sv.edu.bitlab.test.*
import sv.edu.bitlab.test.resources.FormFragment
import sv.edu.bitlab.test.resources.ResultFragment

class CommunicationActivity : AppCompatActivity(),
    FormFragment.OnFragmentInteractionListener, ResultFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comunication)

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById<FrameLayout>(R.id.fragment_container_communication) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return
            }

            // Add the fragment to the 'fragment_container' FrameLayout
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container_communication, FormFragment.newInstance())
                .commit()
        }


    }


    override fun onFragmentInteraction(destination: String) {

        var destinationFragment: Fragment? = null
        val transaction = supportFragmentManager.beginTransaction()

        when (destination) {

            VIEW_FORM -> {
                destinationFragment = FormFragment.newInstance()
                transaction
                    .setCustomAnimations(R.anim.slide_in_left_anim, R.anim.slide_out_right_anim,
                        R.anim.slide_in_left_anim, R.anim.slide_out_right_anim)
            }

            VIEW_RESPONSE -> {
                transaction
                    .setCustomAnimations(
                        R.anim.slide_in_bottom_anim, R.anim.slide_out_top_anim,
                        R.anim.slide_in_left_anim, R.anim.slide_out_right_anim)
                destinationFragment = ResultFragment.newInstance()
            }

            else -> {

            }

        }

        transaction.replace(R.id.fragment_container_communication, destinationFragment!!)
        transaction.addToBackStack(MAIN_VIEW_BACKSTACK)
        // Commit the transaction
        transaction.commit()


        // spinner
        /*
        val optionsAdapter = ArrayAdapter.createFromResource(this, R.array.array_options, R.layout.item_spinner_text)
        optionsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerOptions?.adapter = adapter

        spinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int,
                                                id: Long) { yearItem = year[position] }
                    override fun onNothingSelected(parent: AdapterView<*>?) {} }
        */

    }
}
