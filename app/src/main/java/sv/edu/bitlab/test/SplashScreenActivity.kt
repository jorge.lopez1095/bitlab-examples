package sv.edu.bitlab.test

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import sv.edu.bitlab.test.mvp.MvpActivity

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        // after a 3 seconds, move to main-view.
        Handler().postDelayed({
            startActivity(
                Intent(
                    this@SplashScreenActivity,
                    MvpActivity::class.java
                )
            )
            overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim)
        }, SPLASH_SCREEN_TIME_DELAY.toLong())

    }
}
