package sv.edu.bitlab.test.mvp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_mvp.*
import sv.edu.bitlab.test.R


class MvpActivity : AppCompatActivity(), Operator.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mvp)

        val presenter: Operator.Presenter = OperatorPresenter(this)

        button_calcular.setOnClickListener {
            presenter.operation(text_view_result.text.toString())
        }

        val logo: ImageView = findViewById(R.id.imageViewMvp)

        logo.setImageResource(R.drawable.ic_launcher_background)




    }
    override fun showResult(result: String) {
        text_view_result.text = result
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

}
