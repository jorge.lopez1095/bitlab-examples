package sv.edu.bitlab.test.mvvm

data class AccountModel(var accountId: String, var accountToken: String)