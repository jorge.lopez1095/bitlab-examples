package sv.edu.bitlab.test.mvp

class OperatorModel(private val operatorPresenter: OperatorPresenter) : Operator.Model {

    override fun operation(input: String) {
        val result = input.toDouble() * input.toDouble()

        if (result <= 100.toDouble()) {
            operatorPresenter.showResult(result.toString())
        } else {
            operatorPresenter.showError("Error!")
        }

    }

}